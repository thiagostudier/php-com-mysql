<?php


$conn = new mysqli('localhost', 'root', '', 'php_mysql');

if($conn->connect_errno){

    die('Falha ao conectar: (' . $conn->connect_errno . ')');

}

$sql = 'CREATE TABLE users_teste (
    id int AUTO_INCREMENT PRIMARY KEY,
    email VARCHAR(255) NOT NULL
)';

if(!$conn->query($sql)){
    echo "TABELA JÁ EXISTENTE";
}

echo "<br>";

// $result = $conn->query('INSERT INTO users (email) VALUE ("thiago@thiago.com")');

// FETCH_ASSOC só mostra o resultado uma vez

// $result = $conn->query('SELECT * FROM users');

// echo '<ul>';
//     while ($user = $result->fetch_assoc()){
//         echo '<li>' . $user['email'] . '</li>';
//     }
// echo '</ul>';

// FETCH_ALL reutilizável quando armazenado em uma variável 

$result = $conn->query('SELECT * FROM users');

$users = $result->fetch_all(MYSQLI_ASSOC);

foreach($users as $user){
    echo '<li>' . $user['email'] . '</li>';
}

var_dump($users);
