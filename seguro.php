<?php

$id = $_GET['id'] ?? null;

$conn = new mysqli('localhost', 'root', '', 'php_mysql');

$sql = 'SELECT * FROM users WHERE id = ?';

$stmt = $conn->prepare($sql);

// i = integer
// s = string 
// d = double 
// b = blob

$stmt->bind_param('i', $id);

$stmt->execute();

$result = $stmt->get_result();

$users = $result->fetch_all(MYSQLI_ASSOC);

foreach($users as $user){
    echo $user['id'] . ' ' .$user['email'];
}