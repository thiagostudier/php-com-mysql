<?php

$conn = require 'connection.php';

$id = $_GET['id'] ?? null;

$sql = 'SELECT * FROM users WHERE id = ?';

$stmt = $conn->prepare($sql);

$stmt->bind_param('i', $id);

$stmt->execute();

$result = $stmt->get_result();

$user = $result->fetch_assoc();

if($_SERVER['REQUEST_METHOD'] == 'POST'){

    $email = filter_input(INPUT_POST, 'email') ?? null;

    $sql = 'UPDATE users SET email = ? WHERE id = ?';

    $stmt = $conn->prepare($sql);

    $stmt->bind_param('si', $email, $id);

    $stmt->execute();

    header('location: index.php');
    die();

}

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body>
    
    <h1>Editar usuário</h1>

    <form action="" method="POST">
    
        <input value="<?=$user['email']?>" type="mail" name="email" placeholder="Insira um email" />
        <input type="submit" value="Enviar">
    </form>

    <br>

    <a href="index.php">voltar</a>

</body>
</html>