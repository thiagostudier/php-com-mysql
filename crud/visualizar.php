<?php 

$conn = require 'connection.php';

$id = $_GET['id'] ?? null;

$sql = 'SELECT * FROM users WHERE id = ?';

$stmt = $conn->prepare($sql);

$stmt->bind_param('i', $id);

$stmt->execute();

$result = $stmt->get_result();

$user = $result->fetch_assoc();

?>


<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body>
    <h3><?=$user['email']?></h1>

    <p>Usuário de id: <?=$user['id']?></p>

    <a href="editar.php?id=<?=$user['id']?>">editar</a>
    <a href="remover.php?id=<?=$user['id']?>">remover</a>
    <a href="index.php">voltar</a>
</body>
</html>