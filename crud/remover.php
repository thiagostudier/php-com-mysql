<?php 

$conn = require 'connection.php';

$id = $_GET['id'] ?? null;

$sql = 'DELETE FROM users WHERE id = ?';

$stmt = $conn->prepare($sql);

$stmt->bind_param('i', $id);

$stmt->execute();

header('location: index.php');
die();