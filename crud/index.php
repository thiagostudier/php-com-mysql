<?php 

$conn = require 'connection.php';

$result = $conn->query('SELECT * FROM users');

$users = $result->fetch_all(MYSQLI_ASSOC);

?>


<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body>
    <table>
        <thead>
            <tr>
                <td>#</td>
                <td>email</td>
                <td></td>
            </tr>
        </thead>
        <tbody>

            <?php foreach($users as $user){ ?>

            <tr>
                <td><?=$user['id']?></td>
                <td><?=$user['email']?></td>
                <td>
                    <a href="visualizar.php?id=<?=$user['id']?>">visualizar</a>
                    <a href=""></a>
                </td>
            </tr>

            <?php } ?>

            <a href="criar.php">Adicionar usuário</a>

        </tbody>
    </table>
</body>
</html>