<?php

$conn = require 'connection.php';

if($_SERVER['REQUEST_METHOD'] == 'POST'){

    $email = filter_input(INPUT_POST, 'email') ?? null;

    $sql = 'INSERT INTO users (email) VALUES (?)';

    $stmt = $conn->prepare($sql);

    $stmt->bind_param('s', $email);

    $stmt->execute();

    header('location: index.php');
}

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body>
    
    <h1>Adicionar usuário</h1>

    <form action="" method="POST">
    
        <input type="mail" name="email" placeholder="Insira um email" />
        <input type="submit" value="Enviar">
    </form>

    <br>

    <a href="index.php">voltar</a>

</body>
</html>